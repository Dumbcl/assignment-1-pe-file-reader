/// @file
/// @brief Module that works with pe file's stuff

#ifndef SECTION_EXTRACTOR_PE_FILE_H
#define SECTION_EXTRACTOR_PE_FILE_H

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/// File offset specified
#define MAIN_OFFSET 0x3c
/// Length of name field of section headers
#define NAME_SIZE 8
/// A 4-byte signature that identifies the file as a PE format image file
#define SIGNATURE 0x00004550

#ifdef _MSK_VER
    #pragma pack(push, 1)
#endif

/// @brief COFF File Header. At the beginning of an object file, or immediately after the signature of an image file, is a standard COFF file header in the following format
struct
#ifdef __GNUC__
    __attribute__((packed))
#endif
PEHeader{
    /// The number that identifies the type of target machine
    uint16_t Machine;
    /// The number of sections. This indicates the size of the section table, which immediately follows the headers
    uint16_t NumberOfSections;
    /// The low 32 bits of the number of seconds since 00:00 January 1, 1970 (a C run-time time_t value), which indicates when the file was created
    uint32_t TimeDateStamp;
    /// The file offset of the COFF symbol table, or zero if no COFF symbol table is present. This value should be zero for an image because COFF debugging information is deprecated
    uint32_t PointerToSymbolTable;
    /// The number of entries in the symbol table. This data can be used to locate the string table, which immediately follows the symbol table. This value should be zero for an image because COFF debugging information is deprecated
    uint32_t NumberOfSymbols;
    /// The size of the optional header, which is required for executable files but not for object files. This value should be zero for an object file
    uint16_t SizeOfOptionalHeader;
    /// The flags that indicate the attributes of the file
    uint16_t Characteristics;
};

/// @brief Each row of the section table is, in effect, a section header. This table immediately follows the optional header, if any. ach section header (section table entry) has the following format, for a total of 40 bytes per entry
struct
#if defined __GNUC__
    __attribute__((packed))
#endif
SectionHeader{
    /// An 8-byte, null-padded UTF-8 encoded string
    uint8_t Name[NAME_SIZE];
    /// The total size of the section when loaded into memory. If this value is greater than SizeOfRawData, the section is zero-padded
    uint32_t VirtualSize;
    /// For executable images, the address of the first byte of the section relative to the image base when the section is loaded into memory
    uint32_t VirtualAddress;
    /// The size of the section (for object files) or the size of the initialized data on disk (for image files)
    uint32_t SizeOfRawData;
    /// The file pointer to the first page of the section within the COFF file
    uint32_t PointerToRawData;
    /// The file pointer to the beginning of relocation entries for the section
    uint32_t PointerToRelocations;
    /// The file pointer to the beginning of line-number entries for the section
    uint32_t PointerToLineNumbers;
    /// The number of relocation entries for the section. This is set to zero for executable images
    uint16_t NumberOfRelocations;
    /// The number of line-number entries for the section. This value should be zero for an image because COFF debugging information is deprecated
    uint16_t NumberOfLineNumbers;
    /// The flags that describe the characteristics of the section
    uint32_t Characteristics;
};

/// @brief Structure containing PE file data
struct
#ifdef __GNUC__
    __attribute__((packed))
#endif
PEFile {
    /// Offset to a main PE header
    uint32_t header_offset;
    /// Pe file definition
    uint32_t signature;
    /// Main header of pe file
    struct PEHeader header;
    /// Array of section headers with the size of header.number_of_sections
    struct SectionHeader* section_headers;
};

#ifdef _MSK_VER
    #pragma pack(pop)
#endif


bool PEfile_read (FILE* file_in, struct PEFile* PE_file);

#endif
