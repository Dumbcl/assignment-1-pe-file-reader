/// @file
/// @brief Module that's connected with actions with pe file sections

#ifndef SECTION_EXTRACTOR_SECTIONS_UTIL_H
#define SECTION_EXTRACTOR_SECTIONS_UTIL_H

#include "pe_file.h"

bool section_extract(FILE* file_in, const char* section_name, FILE* file_out);

struct SectionHeader* section_find(const struct PEFile* file, const char* section_name);

bool section_data_write(FILE* file_in, const struct SectionHeader* sectionHeader, FILE* file_out);

#endif
