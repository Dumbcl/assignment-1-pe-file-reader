/// @file 
/// @brief Main application file

#include <sections_util.h>

/// @brief Application entry point
/// @param[in] argc Number of command line arguments
/// @param[in] argv Command line arguments
/// @return 0 in case of success or error code
int main(int argc, char** argv) {
    if (argc != 4) {
        return 0;
    }
    FILE* in_file = fopen(argv[1], "rb");
    FILE* out_file = fopen(argv[3], "wb");
    bool extraction_res = section_extract(in_file, argv[2], out_file);
    return !extraction_res;
}
