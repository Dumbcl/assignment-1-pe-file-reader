/// @file
/// @brief Application file that works with pe file's stuff

#include "pe_file.h"

/// @brief Function that's reading PE file into the structure
/// @param[in] file_in input file
/// @param[in] PEfile defined structure in which is writing pe file headers and stuff
/// @return true in case of success and false in case of some error
bool PEfile_read (FILE* file_in, struct PEFile* PEfile){
    if (!file_in) {
        fprintf(stderr, "Failed PE file reading\n");
        return false;
    }
    else {
        if (fseek(file_in, MAIN_OFFSET, SEEK_SET) != 0){
            fprintf(stderr, "Failed pointer moving\n");
            return false;
        }
        if (!fread(&PEfile->header_offset, sizeof(PEfile->header_offset), 1, file_in)){
            fprintf(stderr, "Failed header offset reading\n");
            return false;
        }
        if (fseek(file_in, PEfile->header_offset, SEEK_SET) != 0){
            fprintf(stderr, "Failed pointer moving\n");
            return false;
        }
        if (!fread(&PEfile->signature, sizeof(PEfile->signature), 1, file_in)){
            fprintf(stderr, "Failed signature reading\n");
            return false;
        }
        if (PEfile->signature != SIGNATURE) {
            fprintf(stderr, "Failed signature definition\n");
            return false;
        }
        if (!fread(&PEfile->header, sizeof(struct PEHeader), 1, file_in)){
            fprintf(stderr, "Failed PEheader reading\n");
            return false;
        }
        if (fseek(file_in, PEfile->header.SizeOfOptionalHeader, SEEK_CUR) != 0){
            fprintf(stderr, "Failed pointer moving\n");
            return false;
        }
        PEfile->section_headers = malloc(PEfile->header.NumberOfSections * sizeof(struct SectionHeader));
        for (uint16_t i = 0; i < PEfile->header.NumberOfSections; i++) {
            if (!fread(&PEfile->section_headers[i], sizeof(struct SectionHeader), 1, file_in)){
                fprintf(stderr, "Failed section header reading\n");
                return false;
            }
        }
        return true;
    }
}


