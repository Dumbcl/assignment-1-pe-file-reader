/// @file
/// @brief Application file that's connected with actions with pe file sections

#include "sections_util.h"


/// @brief Function that finds certain section in pe file
/// @param[in] file pointer to pe file in which there's need to find section
/// @param[in] section_name section, that function have to find
/// @return defined pointer on finded section in case of success and NULL pointer in case of not finding
struct SectionHeader* section_find(const struct PEFile* file, const char* section_name) {
    for (uint16_t i = 0; i < file->header.NumberOfSections; i++) {
        if (memcmp(file->section_headers[i].Name, section_name, strlen(section_name)) == 0) {
            return file->section_headers + i;
        }
    }
    return NULL;
}

/// @brief Function that writes certain section from file to another file
/// @param[in] file_in pointer to pe file in which there's needed section
/// @param[in] sectionHeader pointer to certain section header
/// @param[in] file_out pointer to pe file in which there's need to write section
/// @return true in case of success and false in case of some error
bool section_data_write(FILE* const file_in, const struct SectionHeader* sectionHeader, FILE* file_out) {
    if (sectionHeader) {
        if (fseek(file_in, sectionHeader->PointerToRawData, SEEK_SET) != 0) {
            fprintf(stderr, "Failed pointer moving\n");
            return false;
        }
        char section_data[sectionHeader->SizeOfRawData];
        if (!fread(section_data, sizeof(section_data), 1, file_in)) {
            fprintf(stderr, "Failed section data reading\n");
            return false;
        }
        if (!fwrite(section_data, sizeof(section_data), 1, file_out)) {
            fprintf(stderr, "Failed section data writing\n");
            return false;
        }
        return true;
    }
    fprintf(stderr, "Failed section data writing\n");
    return false;
}

/// @brief Function that extracts certain section from one pe file to another
/// @param[in] file_in pointer to pe file in which there's needed section
/// @param[in] section_name pointer to certain section
/// @param[in] file_out pointer to pe file in which there's need to write section
/// @return true in case of success and false in case of some error
bool section_extract(FILE* file_in, const char* section_name, FILE* file_out){
    bool extraction_res = false;
    struct PEFile PEfile = {0};
    if (PEfile_read(file_in, &PEfile)) {
        struct SectionHeader* section = section_find(&PEfile, section_name);
        extraction_res = section_data_write(file_in, section, file_out);
        free(PEfile.section_headers);
        return extraction_res;
    }
    free(PEfile.section_headers);
    return extraction_res;
}

