var struct_section_header =
[
    [ "Characteristics", "struct_section_header.html#a99ea3dab8420c8b4a4baee36297dd2d7", null ],
    [ "Name", "struct_section_header.html#a892b6d41adcfec7e910fa7b1a802982c", null ],
    [ "NumberOfLineNumbers", "struct_section_header.html#af998180fb759dba9d5da14139eb8dd78", null ],
    [ "NumberOfRelocations", "struct_section_header.html#a881e9fc8c95e422b938ff5e61c81b76a", null ],
    [ "PointerToLineNumbers", "struct_section_header.html#a4f94de5a59e9b3c07c4ac5f74a42dcfa", null ],
    [ "PointerToRawData", "struct_section_header.html#aba324a86a9ce68b36b0362755d924fb7", null ],
    [ "PointerToRelocations", "struct_section_header.html#aa6c623817ba43c030fd4b08f6e458881", null ],
    [ "SizeOfRawData", "struct_section_header.html#a9f42abe9bf601ab29b2510e457863788", null ],
    [ "VirtualAddress", "struct_section_header.html#aaa06e91a66b52ed17240713132347390", null ],
    [ "VirtualSize", "struct_section_header.html#a58e26588631e7030dbdd8a35cf2b8eb2", null ]
];