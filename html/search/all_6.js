var searchData=
[
  ['section_5fdata_5fwrite_0',['section_data_write',['../sections__util_8h.html#a84c01433c1e5e22fc66faf5eb97964f2',1,'section_data_write(FILE *file_in, const struct SectionHeader *sectionHeader, FILE *file_out):&#160;sections_util.c'],['../sections__util_8c.html#a4434c85a0396b3b705936948993898ca',1,'section_data_write(FILE *const file_in, const struct SectionHeader *sectionHeader, FILE *file_out):&#160;sections_util.c']]],
  ['section_5fextract_1',['section_extract',['../sections__util_8h.html#a356ac0088d8104c0bcbd14ef2463dc00',1,'section_extract(FILE *file_in, const char *section_name, FILE *file_out):&#160;sections_util.c'],['../sections__util_8c.html#a356ac0088d8104c0bcbd14ef2463dc00',1,'section_extract(FILE *file_in, const char *section_name, FILE *file_out):&#160;sections_util.c']]],
  ['section_5ffind_2',['section_find',['../sections__util_8h.html#a571a9fdc07e21a10751232ced2e6541f',1,'section_find(const struct PEFile *file, const char *section_name):&#160;sections_util.c'],['../sections__util_8c.html#a571a9fdc07e21a10751232ced2e6541f',1,'section_find(const struct PEFile *file, const char *section_name):&#160;sections_util.c']]],
  ['section_5fheaders_3',['section_headers',['../struct_p_e_file.html#a2563ee8f9da210b01d243089eff2e366',1,'PEFile']]],
  ['sectionheader_4',['SectionHeader',['../struct_section_header.html',1,'']]],
  ['sections_5futil_2ec_5',['sections_util.c',['../sections__util_8c.html',1,'']]],
  ['sections_5futil_2eh_6',['sections_util.h',['../sections__util_8h.html',1,'']]],
  ['signature_7',['signature',['../struct_p_e_file.html#af3e2332349c3eccd779e650b87b1907c',1,'PEFile']]],
  ['signature_8',['SIGNATURE',['../pe__file_8h.html#ae72ce63657183560f1b11dbce5a0a5f6',1,'pe_file.h']]],
  ['sizeofoptionalheader_9',['SizeOfOptionalHeader',['../struct_p_e_header.html#a1d2ae31fd68935846b59777004b60ca2',1,'PEHeader']]],
  ['sizeofrawdata_10',['SizeOfRawData',['../struct_section_header.html#a9f42abe9bf601ab29b2510e457863788',1,'SectionHeader']]]
];
