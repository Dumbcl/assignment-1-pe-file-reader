var searchData=
[
  ['pe_5ffile_2ec_0',['pe_file.c',['../pe__file_8c.html',1,'']]],
  ['pe_5ffile_2eh_1',['pe_file.h',['../pe__file_8h.html',1,'']]],
  ['pefile_2',['PEFile',['../struct_p_e_file.html',1,'']]],
  ['pefile_5fread_3',['PEfile_read',['../pe__file_8h.html#af20ef8f0f995e3624c0f6f31d098c710',1,'PEfile_read(FILE *file_in, struct PEFile *PE_file):&#160;pe_file.c'],['../pe__file_8c.html#a577b8fc6969a19873a90a5c569c60823',1,'PEfile_read(FILE *file_in, struct PEFile *PEfile):&#160;pe_file.c']]],
  ['peheader_4',['PEHeader',['../struct_p_e_header.html',1,'']]],
  ['pointertolinenumbers_5',['PointerToLineNumbers',['../struct_section_header.html#a4f94de5a59e9b3c07c4ac5f74a42dcfa',1,'SectionHeader']]],
  ['pointertorawdata_6',['PointerToRawData',['../struct_section_header.html#aba324a86a9ce68b36b0362755d924fb7',1,'SectionHeader']]],
  ['pointertorelocations_7',['PointerToRelocations',['../struct_section_header.html#aa6c623817ba43c030fd4b08f6e458881',1,'SectionHeader']]],
  ['pointertosymboltable_8',['PointerToSymbolTable',['../struct_p_e_header.html#a4bd49937405075d9974939d40d118512',1,'PEHeader']]]
];
