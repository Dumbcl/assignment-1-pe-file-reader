var searchData=
[
  ['name_0',['Name',['../struct_section_header.html#a892b6d41adcfec7e910fa7b1a802982c',1,'SectionHeader']]],
  ['name_5fsize_1',['NAME_SIZE',['../pe__file_8h.html#a834e9a379307f869a10f4da078be5786',1,'pe_file.h']]],
  ['numberoflinenumbers_2',['NumberOfLineNumbers',['../struct_section_header.html#af998180fb759dba9d5da14139eb8dd78',1,'SectionHeader']]],
  ['numberofrelocations_3',['NumberOfRelocations',['../struct_section_header.html#a881e9fc8c95e422b938ff5e61c81b76a',1,'SectionHeader']]],
  ['numberofsections_4',['NumberOfSections',['../struct_p_e_header.html#a2f30155c9a4c054d5c9030b7af2902d7',1,'PEHeader']]],
  ['numberofsymbols_5',['NumberOfSymbols',['../struct_p_e_header.html#acc70d304f460fd41b68616c5581bfa17',1,'PEHeader']]]
];
