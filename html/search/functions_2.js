var searchData=
[
  ['section_5fdata_5fwrite_0',['section_data_write',['../sections__util_8h.html#a84c01433c1e5e22fc66faf5eb97964f2',1,'section_data_write(FILE *file_in, const struct SectionHeader *sectionHeader, FILE *file_out):&#160;sections_util.c'],['../sections__util_8c.html#a4434c85a0396b3b705936948993898ca',1,'section_data_write(FILE *const file_in, const struct SectionHeader *sectionHeader, FILE *file_out):&#160;sections_util.c']]],
  ['section_5fextract_1',['section_extract',['../sections__util_8h.html#a356ac0088d8104c0bcbd14ef2463dc00',1,'section_extract(FILE *file_in, const char *section_name, FILE *file_out):&#160;sections_util.c'],['../sections__util_8c.html#a356ac0088d8104c0bcbd14ef2463dc00',1,'section_extract(FILE *file_in, const char *section_name, FILE *file_out):&#160;sections_util.c']]],
  ['section_5ffind_2',['section_find',['../sections__util_8h.html#a571a9fdc07e21a10751232ced2e6541f',1,'section_find(const struct PEFile *file, const char *section_name):&#160;sections_util.c'],['../sections__util_8c.html#a571a9fdc07e21a10751232ced2e6541f',1,'section_find(const struct PEFile *file, const char *section_name):&#160;sections_util.c']]]
];
