var pe__file_8h =
[
    [ "PEHeader", "struct_p_e_header.html", "struct_p_e_header" ],
    [ "SectionHeader", "struct_section_header.html", "struct_section_header" ],
    [ "PEFile", "struct_p_e_file.html", "struct_p_e_file" ],
    [ "MAIN_OFFSET", "pe__file_8h.html#a9cc3f03c591339e6f34f2e15e4271b87", null ],
    [ "NAME_SIZE", "pe__file_8h.html#a834e9a379307f869a10f4da078be5786", null ],
    [ "SIGNATURE", "pe__file_8h.html#ae72ce63657183560f1b11dbce5a0a5f6", null ],
    [ "PEfile_read", "pe__file_8h.html#af20ef8f0f995e3624c0f6f31d098c710", null ]
];